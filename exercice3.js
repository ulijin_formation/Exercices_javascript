//exercice 3 retourner la somme de deux nombres
// les fonctions permettent de ne pas répeter du code

debugger;//

function addition(a, b) {
    var result = a + b;
    return result;
}

console.log("somme de 15 et 10", addition(15, 10));
console.log("somme de 15 et 10", addition(100, 100));
console.log("somme de 15 et 10", addition(-15, 15));


//var est la declaration initiale de javascript
// il vaut mieux déclarer avec let, il execute l'instruction uniquement quand il la voit
//Pour var la portée est declaré pour tout le fichier

//on peut  declarer les addition par des variables cf screenshoot  par soucis de visibilité c'est bien de declarer la variable