//créez une fonction qui prend la tension et le courant et renvoie la puissance calculée
function circuitPower(voltage, current) {
    return voltage * current;
    
}
console.log(circuitPower(110, 3));
console.log(circuitPower(230, 10));
console.log(circuitPower(480, 20));